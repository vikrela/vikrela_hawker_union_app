package vikrela.com.vikrelahawkerunion.dto;

/**
 * Created by Harsh on 6/29/2017.
 */
public class DepositDTO {
    String date;
    String amount;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
