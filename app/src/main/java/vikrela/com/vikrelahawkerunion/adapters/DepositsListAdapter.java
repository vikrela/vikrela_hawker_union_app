package vikrela.com.vikrelahawkerunion.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import vikrela.com.vikrelahawkerunion.R;
import vikrela.com.vikrelahawkerunion.dto.DepositDTO;

/**
 * Created by Harsh on 6/29/2017.
 */
public class DepositsListAdapter extends BaseAdapter {
    private Context context;
    private List<DepositDTO> depositDTOList;
    private LayoutInflater inflater;

    public DepositsListAdapter(Context context, List<DepositDTO> depositDTOList) {
        this.context = context;
        this.depositDTOList = depositDTOList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return depositDTOList.size();
    }

    @Override
    public Object getItem(int position) {
        return depositDTOList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtDate, txtAmount;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.deposit_custom_row, parent, false);
            txtDate = (TextView) convertView.findViewById(R.id.txtDate);
            txtAmount = (TextView) convertView.findViewById(R.id.txtAmount);
            txtDate.setText(depositDTOList.get(position).getDate());
            txtAmount.setText(depositDTOList.get(position).getAmount());
        }
        return convertView;
    }
}
