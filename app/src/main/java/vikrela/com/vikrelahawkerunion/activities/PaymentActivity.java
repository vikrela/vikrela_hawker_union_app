package vikrela.com.vikrelahawkerunion.activities;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.vikrelahawkerunion.R;

public class PaymentActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtPaymentMode)
    TextView txtPaymentMode;
    @Bind(R.id.txtPaymentHint)
    TextView txtPaymentHint;
    @Bind(R.id.btnSubmit)
    Button btnSubmit;
    @Bind(R.id.radiogroupPayment)
    RadioGroup radioGroupPayment;
    @Bind(R.id.radioAutoDebit)
    RadioButton radioAutoDebit;
    @Bind(R.id.radioCash)
    RadioButton radioCash;
    @Bind(R.id.rlAmount)
    RelativeLayout rlAmount;
    @Bind(R.id.txtDebitInfo)
    TextView txtDebitInfo;
    @Bind(R.id.txtPaymentType)
    TextView txtPaymentType;
    @Bind(R.id.radioGroupPeriod)
    RadioGroup radioGroupPeriod;
    @Bind(R.id.rdbtnMonthly)
    RadioButton rdbtnMonthly;
    @Bind(R.id.rdbtnQuarterly)
    RadioButton rdbtnQuarterly;
    @Bind(R.id.rdbtnYearly)
    RadioButton rdbtnYearly;
    Bundle bundle1, bundle2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getString(R.string.payment));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtPaymentMode.setPaintFlags(txtPaymentMode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtPaymentType.setPaintFlags(txtPaymentMode.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtPaymentHint.setPaintFlags(txtPaymentHint.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Bundle bundle = getIntent().getExtras();
        if (null != bundle) {
            if (bundle.containsKey("daily_deposit") && bundle.getString("daily_deposit").equals("yes")) {
                //radioAutoDebit.setEnabled(false);
                txtPaymentType.setVisibility(View.INVISIBLE);
                radioGroupPayment.setVisibility(View.INVISIBLE);
                radioGroupPeriod.setVisibility(View.INVISIBLE);
                txtPaymentMode.setVisibility(View.INVISIBLE);
                txtDebitInfo.setVisibility(View.GONE);
                btnSubmit.setText("Collect Cash");
                txtPaymentHint.setVisibility(View.VISIBLE);
                rlAmount.setVisibility(View.VISIBLE);
                bundle2 = new Bundle();
                bundle2.putString("daily_deposit", "submit");
            }
            if (bundle.containsKey("membership_fee") && bundle.getString("membership_fee").equals("yes")) {
                //radioAutoDebit.setEnabled(true);
                //txtDebitInfo.setVisibility(View.VISIBLE);
                radioGroupPeriod.setVisibility(View.VISIBLE);
                txtPaymentHint.setVisibility(View.INVISIBLE);
                rlAmount.setVisibility(View.INVISIBLE);
                bundle1 = new Bundle();
                bundle1.putString("membership_fee", "submit");
            }
        }
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentActivity.this, OtpActivity.class);
                if (null != bundle2) {
                    intent.putExtras(bundle2);
                }
                if (null != bundle1) {
                    intent.putExtras(bundle1);
                }
                startActivity(intent);
            }
        });

        txtDebitInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PaymentActivity.this, BankDetailsActivity.class));
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
