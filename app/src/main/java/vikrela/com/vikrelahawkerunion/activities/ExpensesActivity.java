package vikrela.com.vikrelahawkerunion.activities;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.vikrelahawkerunion.R;

public class ExpensesActivity extends AppCompatActivity implements OnChartValueSelectedListener {

    protected String[] mMonths = new String[]{
            "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    };
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.txtJanuaryDeposit)
    TextView txtJanDeposit;
    @Bind(R.id.txtFebruaryDeposit)
    TextView txtFebDeposit;
    @Bind(R.id.txtMarchDeposit)
    TextView txtMarDeposit;
    @Bind(R.id.txtAprilDeposit)
    TextView txtAprDeposit;
    @Bind(R.id.txtMayDeposit)
    TextView txtMayDeposit;
    @Bind(R.id.txtJuneDeposit)
    TextView txtJunDeposit;
    @Bind(R.id.txtJulyDeposit)
    TextView txtJulDeposit;
    @Bind(R.id.txtAugustDeposit)
    TextView txtAugDeposit;
    @Bind(R.id.txtSeptemberDeposit)
    TextView txtSeptDeposit;
    @Bind(R.id.txtOctoberDeposit)
    TextView txtOctDeposit;
    @Bind(R.id.txtNovemberDeposit)
    TextView txtNovDeposit;
    @Bind(R.id.txtDecemberDeposit)
    TextView txtDecDeposit;
    private PieChart mChart;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Yearly Deposits");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        mChart = (PieChart) findViewById(R.id.chart1);
        mChart.setUsePercentValues(false);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 5, 5, 5);
        mChart.setDragDecelerationFrictionCoef(0.95f);
        mChart.setDrawSliceText(false);
        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(Color.WHITE);
    /*    mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);
    */
        mChart.setHoleRadius(70f);
        mChart.setRotationAngle(0);
        mChart.setTransparentCircleRadius(0f);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(true);
        mChart.setHighlightPerTapEnabled(true);
        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);

        setData(12);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);

        mChart.getLegend().setEnabled(false);
        mChart.setCenterText("Total Deposits\nRs. 430");
        mChart.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf"));
        mChart.setCenterTextSize(20f);

        // entry label styling
/*
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf"));
        mChart.setEntryLabelTextSize(10f);
*/

    }


    private void setData(int count) {


        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        entries.add(new PieEntry(80, mMonths[0]));
        txtJanDeposit.setText("80");
        entries.add(new PieEntry(30, mMonths[1]));
        txtFebDeposit.setText("30");
        entries.add(new PieEntry(20, mMonths[2]));
        txtMarDeposit.setText("20");
        entries.add(new PieEntry(13, mMonths[3]));
        txtAprDeposit.setText("20");
        entries.add(new PieEntry(40, mMonths[4]));
        txtMayDeposit.setText("40");
        entries.add(new PieEntry(35, mMonths[5]));
        txtJunDeposit.setText("35");
        entries.add(new PieEntry(11, mMonths[6]));
        txtJulDeposit.setText("15");
        entries.add(new PieEntry(36, mMonths[7]));
        txtAugDeposit.setText("36");
        entries.add(new PieEntry(56, mMonths[8]));
        txtSeptDeposit.setText("56");
        entries.add(new PieEntry(24, mMonths[9]));
        txtOctDeposit.setText("24");
        entries.add(new PieEntry(48, mMonths[10]));
        txtNovDeposit.setText("40");
        entries.add(new PieEntry(34, mMonths[11]));
        txtDecDeposit.setText("34");

        PieDataSet dataSet = new PieDataSet(entries, "Monthly Expenses");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(2f);
        dataSet.setIconsOffset(new MPPointF(0, 20));
        dataSet.setSelectionShift(9f);
        // add a lot of colors
/*

        ArrayList<Integer> colors = new ArrayList<Integer>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());
*/

        //dataSet.setSelectionShift(0f);

        dataSet.setColors(ColorTemplate.rgb("#FFB900"), ColorTemplate.rgb("#107C10"), ColorTemplate.rgb("#881798"),
                ColorTemplate.rgb("#00B7C3"), ColorTemplate.rgb("#F7630C"), ColorTemplate.rgb("#a0522d"),
                ColorTemplate.rgb("#E81123"), ColorTemplate.rgb("#0063B1"), ColorTemplate.rgb("#E3008C"),
                ColorTemplate.rgb("#2D7D91"), ColorTemplate.rgb("#4C4A48"), ColorTemplate.rgb("#7A942E"));
        //dataSet.setColors(ColorTemplate.JOYFUL_COLORS);
        PieData data = new PieData(dataSet);
        data.setDrawValues(false);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(09f);
        data.setValueTextColor(Color.WHITE);
        data.setValueTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf"));
        mChart.setData(data);
        // undo all highlights
        mChart.highlightValues(null);
        mChart.invalidate();
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

        if (e == null)
            return;
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", index: " + h.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
        Intent intent = new Intent(ExpensesActivity.this, DailyDepositsActivity.class);
        intent.putExtra("month", e.getY());
        startActivity(intent);
    }

    @Override
    public void onNothingSelected() {
        Log.i("PieChart", "nothing selected");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
