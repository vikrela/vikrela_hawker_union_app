package vikrela.com.vikrelahawkerunion.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.vikrelahawkerunion.R;

public class OtpActivity extends AppCompatActivity {

    @Bind(R.id.etOtp)
    EditText etOtp;
    @Bind(R.id.btnSubmitOtp)
    Button btnSubmitOtp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        final Bundle bundle = getIntent().getExtras();

        btnSubmitOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MaterialDialog dialog1 = new MaterialDialog.Builder(OtpActivity.this)
                        .title("")
                        .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf")
                        .customView(R.layout.dialog_custom_view, true)
                        .cancelable(false)
                        .show();

                TextView txtThanks = (TextView) dialog1.findViewById(R.id.txtThanks);
                TextView txtRentDone = (TextView) dialog1.findViewById(R.id.txtDone);
                Button btnHome = (Button) dialog1.findViewById(R.id.btnHome);
                TextView txtReceipt = (TextView) dialog1.findViewById(R.id.txtViewReceipt);
                final Button btnContinue = (Button) dialog1.findViewById(R.id.btnContinue);
                if (null != bundle) {
                    if (bundle.containsKey("membership_fee") && bundle.getString("membership_fee").equals("submit")) {
                        btnContinue.setText("Collect Deposit");
                    }
                    if (bundle.containsKey("daily_deposit") && bundle.getString("daily_deposit").equals("submit")) {
                        btnContinue.setText("Membership Fee");
                    }
                }
                Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Whitney-Semibold-Bas.otf");
                txtThanks.setTypeface(typeface);
                txtRentDone.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/Whitney-Book-Bas.otf"));
                btnContinue.setTypeface(typeface);
                btnHome.setTypeface(typeface);
                btnHome.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OtpActivity.this, DashboardActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(OtpActivity.this);
                    }
                });

                btnContinue.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OtpActivity.this, HawkerInfoActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(OtpActivity.this);
                    }
                });
                txtReceipt.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(OtpActivity.this, ReceiptActivity.class);
                        startActivity(intent);
                        ActivityCompat.finishAffinity(OtpActivity.this);
                    }
                });
            }
        });
    }
}
