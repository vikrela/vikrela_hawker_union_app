package vikrela.com.vikrelahawkerunion.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vikrela.com.vikrelahawkerunion.R;
import vikrela.com.vikrelahawkerunion.ui.Snackbar;

public class BankDetailsActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.etBankName)
    EditText etBankName;
    @Bind(R.id.etStateName)
    EditText etStateName;
    @Bind(R.id.etDistrict)
    EditText etDistrict;
    @Bind(R.id.etBranchName)
    EditText etBranchName;
    @Bind(R.id.etAccountName)
    EditText etAccountName;
    @Bind(R.id.etAccountNumber)
    EditText etAccountNumber;
    @Bind(R.id.etIFSCCode)
    EditText etIFSCCode;
    @Bind(R.id.btnSubmitBankDetails)
    Button btnSubmitBankDetails;

    @OnClick(R.id.btnSubmitBankDetails)
    void proceed() {

        // validations are applied from here for filling bank details
        if (etBankName.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_bank_name));
            etBankName.requestFocus();
            etBankName.setError(getString(R.string.bank_name));
            return;
        }
        if (etStateName.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_state_name));
            etStateName.setError(getString(R.string.state_name));
            etStateName.requestFocus();
            return;
        }
        if (etDistrict.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_district_name));
            etDistrict.requestFocus();
            etDistrict.setError(getString(R.string.district));
            return;
        }
        if (etBranchName.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_branch_name));
            etBranchName.requestFocus();
            etBranchName.setError(getString(R.string.branch_name));
            return;
        }
        if (etAccountName.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_account_name));
            etAccountName.requestFocus();
            etAccountName.setError(getString(R.string.account_name));
            return;
        }
        if (etAccountNumber.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_account_number));
            etAccountNumber.requestFocus();
            etAccountNumber.setError(getString(R.string.account_number));
            return;
        }
        if (etIFSCCode.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.enter_ifsc_code));
            etIFSCCode.requestFocus();
            etIFSCCode.setError(getString(R.string.ifsc_code));
            return;
        } else {
            // if all validations are true then proceed
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_details);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Bank Details");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
