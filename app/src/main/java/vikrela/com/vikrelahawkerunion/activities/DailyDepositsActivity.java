package vikrela.com.vikrelahawkerunion.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import vikrela.com.vikrelahawkerunion.R;
import vikrela.com.vikrelahawkerunion.adapters.DepositsListAdapter;
import vikrela.com.vikrelahawkerunion.dto.DepositDTO;

public class DailyDepositsActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.listDaily)
    ListView listDaily;
    DepositsListAdapter adapter;
    List<DepositDTO> depositDTOList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_deposits);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Daily Deposits");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new DepositsListAdapter(DailyDepositsActivity.this, getData());
        listDaily.setAdapter(adapter);
    }

    public List<DepositDTO> getData() {
        depositDTOList = new ArrayList<>();
        String[] date = {"1-Jan", "2-Jan", "3-Jan", "4-Jan", "5-Jan", "8-Jan", "10-Jan", "11-Jan", "15-Jan", "16-Jan", "17-Jan", "18-Jan"};
        String[] amount = {"Rs. 10", "Rs. 15", "Rs. 25", "Rs. 20", "Rs. 40", "Rs. 10", "Rs. 36", "Rs. 25", "Rs. 60", "Rs. 10", "Rs. 24", "Rs. 20"};

        for (int i = 0; i < date.length; i++) {
            DepositDTO dto = new DepositDTO();
            dto.setDate(date[i]);
            dto.setAmount(amount[i]);
            depositDTOList.add(dto);
        }
        return depositDTOList;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
