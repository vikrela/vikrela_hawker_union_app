package vikrela.com.vikrelahawkerunion.fragments;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import vikrela.com.vikrelahawkerunion.R;
import vikrela.com.vikrelahawkerunion.activities.HawkerInfoActivity;
import vikrela.com.vikrelahawkerunion.activities.ScanQRCodeActivity;
import vikrela.com.vikrelahawkerunion.ui.Snackbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {

    @Bind(R.id.etLicenseNumber)
    EditText etLicenseNumber;
    @Bind(R.id.btnEnter)
    Button btnEnter;
    @Bind(R.id.btnScanQR)
    ImageView btnScanQR;
    View parentView;
    SharedPreferences preferences;

    @OnClick(R.id.btnScanQR)
    void scanQR() {
        startActivity(new Intent(getActivity(), ScanQRCodeActivity.class));
    }

    @OnClick(R.id.btnEnter)
    void info() {
        if (etLicenseNumber.getText().toString().equals("")) {
            Snackbar.show(getActivity(), getString(R.string.empty_field_error));
            return;
        } else {
            Intent intent = new Intent(getActivity(), HawkerInfoActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("license", etLicenseNumber.getText().toString());
            intent.putExtras(bundle);
            startActivity(intent);
            preferences.edit().putString("scan_qr", "no").commit();
            preferences.edit().putString("hawker_info_id", etLicenseNumber.getText().toString()).commit();
        }
    }

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, parentView);
        preferences = getActivity().getSharedPreferences("Virkela", Context.MODE_PRIVATE);
        populate();
        return parentView;
    }

    private void populate() {
        etLicenseNumber.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
    }
}
