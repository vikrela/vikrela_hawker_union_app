package vikrela.com.vikrelahawkerunion.constants;

/**
 * Created by Harsh on 6/25/2017.
 */
public interface NetworkConstants {
    public String NETWORK_IP = "http://v16he2v4.esy.es/android/";
    public String INFORMATION_URL = NETWORK_IP + "/retriveHawkerData.php";
    public String INFORMATION_URL_ID = NETWORK_IP + "/retriveHawkerDataFromLisenceNo.php";

}
